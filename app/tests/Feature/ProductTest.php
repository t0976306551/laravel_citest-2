<?php

namespace Tests\Feature\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_create_product(): void
    {
        $productData = [
            'name' => 'test',
            'price' => 100
        ];
        $response = $this->post('/api/product', $productData);
        $response->assertStatus(200);
        $this->assertEquals('create success', $response->json('message'));
        $this->assertEquals(true, $response['success']);
    }

    public function test_create_produc_name_be_empty(): void
    {
        $productData = [
            'name' => '',
            'price' => 100
        ];
        $response = $this->post('/api/product', $productData);
        $response->assertStatus(409);
        $this->assertEquals('product name can not be empty', $response->json('message'));
        $this->assertEquals(false, $response['success']);
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
