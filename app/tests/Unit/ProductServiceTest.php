<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\ProductService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;

class ProductServiceTest extends TestCase
{
    use RefreshDatabase;
    public function setUp(): void
    {
        parent::setUp();
    }
    public function test_product_create()
    {
        $productServiceMock = Mockery::mock(ProductService::class);

        $productServiceMock->shouldReceive('create')
            ->once()
            ->with(['name' => 'Test Product', 'price' => 100])
            ->andReturn([
                'success' => true,
                'message' => 'create success',
                'status' => 200
            ]);

        $response = $productServiceMock->create(['name' => 'Test Product', 'price' => 100]);
        $this->assertTrue($response['success']);
        $this->assertEquals('create success', $response['message']);
        $this->assertEquals(200, $response['status']);
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
