<?php

namespace App\Services;

use App\Models\Product;

class ProductService
{
    public static function create($data)
    {
        try {

            if ($data['name'] == '') {
                $reponseData = [
                    'success' => false,
                    'message' => 'product name can not be empty',
                    'status' => 409
                ];
                return $reponseData;
            }

            if ($data['price'] == null) {
                $reponseData = [
                    'success' => false,
                    'message' => 'product price can not be null',
                    'status' => 409
                ];

                return $reponseData;
            }

            $createProductData = [
                'name' => $data['name'],
                'price' => $data['price']
            ];

            $createStatus = Product::create($createProductData);
            if ($createStatus) {
                $reponseData = [
                    'success' => true,
                    'message' => 'create success',
                    'status' => 200
                ];
            } else {
                $reponseData = [
                    'success' => false,
                    'message' => 'create fail',
                    'status' => 400
                ];
            }
            return $reponseData;
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while Product',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }
}
