<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;

class ProductController extends Controller
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function create(Request $request)
    {
        $data = [
            'name' => $request->input('name') ?? '',
            'price' => $request->input('price') ?? null
        ];

        // return response()->json($data);
        $result = $this->productService->create($data);
        return response()->json($result, $result['status']);
    }
}
